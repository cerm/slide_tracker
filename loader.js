/**
 * Created by Anders on 13/07/16.
 */
var gitHack = 'https://bb.githack.com/anhesr/api/raw/master/AbstractInterface.js';
var interface;

$.getScript(gitHack, function () {
    console.log('interface loaded');
    interface = new AbstractInterface(function () {
        console.log('callback');
    });
    console.log(interface);
});